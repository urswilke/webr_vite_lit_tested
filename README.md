This repo exists to showcase how to add WebdriverIO tests (with chrome & firefox CI runners) to a template repo created with [this](https://github.com/hrbrmstr/create-webr-vite-lit).


In order to run the test on your machine, this should do it:

* clone this repo
* run `npm install`
* run `npm run test` (firefox)
* or run `npm run test -- --chrome` if you prefer chrome

The code resulting from running 

```
npx create-webr-vite-lit my-webr-project
```

(see initial commit in this repo) is licensed with MIT.
