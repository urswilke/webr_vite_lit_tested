import { html, render } from 'lit'
import { $, expect } from '@wdio/globals'

import '../status-message.js'

render(
    html`<status-message></status-message>`,
    document.body
)
const status_message_el = await $('status-message')
describe('Check if tests are run', () => {
    it('should have text "Webr"', async () => {
        await expect(status_message_el).toHaveText('WebR')
    })

})
