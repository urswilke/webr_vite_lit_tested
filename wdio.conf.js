const chrome_capabilities = {
    browserName: 'chrome',
    'goog:chromeOptions': {
        args: [
            '--headless',
            '--no-sandbox',
            '--disable-gpu',
            '--disable-background-networking',
            '--enable-features=NetworkService,NetworkServiceInProcess',
            '--disable-background-timer-throttling',
            '--disable-backgrounding-occluded-windows',
            '--disable-breakpad',
            '--disable-client-side-phishing-detection',
            '--disable-component-extensions-with-background-pages',
            '--disable-default-apps',
            '--disable-dev-shm-usage',
            '--disable-extensions',
            '--disable-features=TranslateUI,BlinkGenPropertyTrees',
            '--disable-hang-monitor',
            '--disable-ipc-flooding-protection',
            '--disable-popup-blocking',
            '--disable-prompt-on-repost',
            '--disable-renderer-backgrounding',
            '--disable-sync',
            '--force-color-profile=srgb',
            '--metrics-recording-only',
            '--no-first-run',
            '--enable-automation',
            '--password-store=basic',
            '--use-mock-keychain',
        ]
    }
};

const firefox_capabilities = {
    browserName: 'firefox',
    'moz:firefoxOptions': {
        args: [
            '-headless'
        ]
    }
};
console.log(process.argv)
let capabilities;
switch (true) {
    case process.argv.includes('--firefox'):
        capabilities = firefox_capabilities;
        break;
    case process.argv.includes('--chrome'):
        capabilities = chrome_capabilities;
        break;
    case process.argv.includes('--edge'):
    default:
        console.log('no browser specified - using firefox');
        capabilities = firefox_capabilities;
        break;
}


export const config = {
    runner: 'browser',
    specs: [
        './src/**/*.test.js'
    ],
    exclude: [],
    maxInstances: 10,
    capabilities: [capabilities],
    logLevel: 'info',
    bail: 0,
    baseUrl: '',
    waitforTimeout: 10000,
    connectionRetryTimeout: 120000,
    connectionRetryCount: 3,
    framework: 'mocha',
    reporters: ['spec'],
    mochaOpts: {
        ui: 'bdd',
        timeout: 60000
    },
}
